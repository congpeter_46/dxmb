const React = require("react-native");
const { Platform } = React;

export default {
  container: {
    backgroundColor: "#FBFAFA"
  },
  textTitle : {
    color:"white",
    fontSize:20,
    marginTop:20,
    fontWeight:"bold"
  },
  inputStyle :{
    color:"#999999"
  },
  viewStatusBar:{
    height: (Platform.OS === "ios") ? 1 : 24,
  }
};
