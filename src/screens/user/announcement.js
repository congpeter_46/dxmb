import React, { Component } from "react";
import { Image, Dimensions, TouchableOpacity } from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Card,
  CardItem,
  Thumbnail,
  Icon,
  Text,
  Right,
  Body,
  Left,
  View
} from "native-base";
import styles from "../card/styles";

const deviceWidth = Dimensions.get("window").width;
const datas = [
  {
    route: "NewsDetails",
    text: "NewsDetails"
  },
  {
    route: "IconActionSheet",
    text: "Icon ActionSheet"
  }
];

class AnnouncementList extends Component {
  render() {
    return (
      <Container style={{ backgroundColor: "#fff" }}>
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.goBack()}
            >
              <Icon name="arrow-back"/>
            </Button>
          </Left>
          <Body>
          <Title>Tin nội bộ</Title>
          </Body>
          <Right/>
        </Header>
        <Content>
          <TouchableOpacity>
          <Card style={styles.mb}>
            <CardItem bordered>
              <Left>
                <Thumbnail square source={require("../../../assets/logo-sm.jpg")} />
                <Body>
                <Text>Lịch nghỉ hè của công ty năm 2018, địa điểm hồ Thiền Quang - Hà Nội</Text>
                <Text note><Icon name="calendar" style={{fontSize:13}} /> 14/04/2017</Text>
                </Body>
              </Left>
            </CardItem>

            <CardItem style={{ paddingVertical: 0 }}>
              <Body>
              <Image
                style={styles.cardImage}
                source={require("../../../assets/DXMB/1.jpg") }
              />
              <Text> NativeBase
                builds a layer on top of React Native that provides you with
                basic set of components for mobile application development.
              </Text>
              </Body>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent>
                  <Icon name="pricetag" />
                  <Text>Phòng HCNS</Text>
                </Button>
              </Left>
              <Right>
                <Button transparent>
                  <Icon name="chatbubbles" />
                  <Text>20 bình luận</Text>
                </Button>
              </Right>
            </CardItem>
          </Card>
          <Card style={styles.mb}>
            <CardItem bordered>
              <Left>
                <Thumbnail square source={require("../../../assets/DXMB/2.jpg")} />
                <Body>
                <Text>Lịch nghỉ hè của công ty năm 2018, địa điểm hồ Thiền Quang - Hà Nội</Text>
                <Text note>
                  <Text style={{grid:1}}>14/04/2017</Text>
                  <Text style={{grid:1,selfAlign:"right", justifyContent: "right"}}><Icon name="chatbubbles" style={{fontSize:13}} /> 20</Text>
                </Text>
                </Body>
              </Left>
            </CardItem>
          </Card>
          </TouchableOpacity>
        </Content>
      </Container>
    );
  }
}

export default AnnouncementList;
