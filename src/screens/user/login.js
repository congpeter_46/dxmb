import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Input,
  Item,
  Left,
  Body,
  Spinner
} from "native-base";
import styles from "./styles";
import { Image, Text, TouchableOpacity, View } from "react-native";

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {loading: true};
  }
  _spinning = ()=>{
    return (<Spinner color={"blue"}/>);
  };

  render() {
    return (
      <Container style={{backgroundColor:"#015da6"}}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back"/>
              <Image source={require("../../../assets/logo.png")} style={{width:40,height:30, marginLeft:16,justifyContent:"center"}}/>
            </Button>
          </Left>
          <Body>
          <Title>Dat Xanh Mien Bac</Title>
          </Body>
        </Header>
        <Content contentContainerStyle={{
          flex:1,
          padding:16
        }}>
          <Text style={styles.textTitle}>ĐĂNG NHẬP</Text>
          <Item regular style={{backgroundColor:"white", marginTop:20,padding:0}}>
            <Icon active name="md-person" style={{color:"#999999"}}/>
            <Input keyboardType={"email-address"} placeholder="Email" style={styles.inputStyle}  />
          </Item>
          <Item regular style={{backgroundColor:"white" , marginTop:20}}>
            <Icon active name="md-lock"  style={{color:"#999999"}}/>
            <Input type="password" secureTextEntry = {true} placeholder="Mật Khẩu" style={styles.inputStyle} />
          </Item>

          <View style={{backgroundColor:"#f99947", marginTop:30 , padding:10 , justifyContent:"center",alignContent:"center"}}>
            <Text style={{alignContent:"center",alignItems:"center",justifyContent:"center",textAlign: "center" ,color:"white"}}>ĐĂNG NHẬP</Text>
          </View>
          <TouchableOpacity onPress={()=>{
            this.props.navigation.navigate("Register");
          }}>
            <Text style={{alignContent:"center" ,textAlign: "center" , marginTop:30,color:"white", textDecorationLine:"underline" }}>Đăng ký làm CTV</Text>
          </TouchableOpacity>
        </Content>
      </Container>
    );
  }
}
