import React, { Component } from "react";
import {
  Button,
  Footer,
  FooterTab,
  Text,
  Icon
} from "native-base";
import styles from "./styles";
import Home from "../home/index";
import ProjectList from "../Projects/index";

class MyFooter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: "home"
    };
  }
  _activeTab = (tab) => {
    this.setState({
      activeTab: tab
    });
    console.log(this.props);
    console.log("==========Props=======");
  };
  render() {
    return (
        <Footer>
          <FooterTab>
            <Button active={this.state.activeTab === "home"} onPress={() => this._activeTab("home") }>
              <Icon active={this.state.activeTab === "home"} name="home" />
              <Text>Trang chủ</Text>
            </Button>
            <Button active={this.state.activeTab === "projects"} onPress={() => this._activeTab("projects") }>
              <Icon active={this.state.activeTab === "projects"} name="list" />
              <Text>Dự án</Text>
            </Button>
            <Button active={this.state.activeTab === "person"} onPress={() => this._activeTab("person")}>
              <Icon active={this.state.activeTab === "person"} name="person" />
              <Text>Cá nhân</Text>
            </Button>
          </FooterTab>
        </Footer>
    );
  }
}

export default MyFooter;
