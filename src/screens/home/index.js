import React, { Component } from "react";
import { Image, StatusBar, Text, TouchableOpacity } from "react-native";
import {
  Card,
  CardItem,
  Container,
  Content,
  Header,
  H3,
  FooterTab,
  Footer,
  Title,
  Tabs,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Input,
  Item,
  List,
  ListItem,
  Thumbnail,
  ScrollableTab,
  Tab,
  View
} from "native-base";
import Swiper from "react-native-swiper";

import TabContact from "./tabContact";
import TabAbout from "./tabAbout";
import TabNews from "./tabNews";
import TabServiceCare from "./tabServiceCare";

import ProjectList from "../Projects/index";
import AnnouncementList from "../user/announcement";

import styles from "./styles";

// const launchscreenBg = require("../../../assets/launchscreen-bg.png");
// const launchscreenLogo = require("../../../assets/logo-kitchen-sink.png");

const datas = [
  {
    img: require("../../../assets/DXMB/1.jpg"),
    title: " Cách chọn nhà trong thời đại ăn phải ngon, mặc phải đẹp",
    date: "12/06/2017",
    id: "1"
  },
  {
    img: require("../../../assets/DXMB/2.jpg"),
    title: "Khi mua nhà không còn là việc mua trên giấy tờ",
    date: "11/09/2017",
    id: "2"
  },
  {
    img: require("../../../assets/DXMB/3.jpg"),
    title: "Bất động sản đầu năm 2018: Đất vàng Hồ Tây tiếp tục hút khách",
    date: "12/01/2018",
    id: "3"
  },
  {
    img: require("../../../assets/DXMB/4.jpg"),
    title: "Hiểu đúng về condotel: Để khách hàng khỏi thiệt",
    date: "12/06/2017",
    id: "4"
  }
];
const projects = [
  {
    img: require("../../../assets/DXMB/3.jpg"),
    title: "Chung cư cao cấp Imperial Plaza",
    address: "360 Giải Phóng",
    price: "25.5 triệu/m2",
    id: "1"
  },
  {
    img: require("../../../assets/DXMB/1.jpg"),
    title: "Chung cư Hateco Apollo Xuân Phương",
    address: "Phường Phương Canh, Nam Từ Liêm",
    price: "1.2 tỷ/căn",
    id: "2"
  },
  {
    img: require("../../../assets/DXMB/4.jpg"),
    title: "Chung cư TNR Sky Park",
    address: "136 Hồ Tùng Mậu, Bắc Từ Liêm, Hà Nội",
    price: "24 triệu/m2",
    id: "3"
  },
  {
    img: require("../../../assets/DXMB/2.jpg"),
    title: "Chung cư Roman Plaza Hải Phát",
    address: "Phường Đại Mỗ, Hà Nội",
    price: "27 triệu/m2",
    id: "4"
  }
];

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
      active1: false,
      active2: false,
      active3: false,
      activeTab: "home"
    };
  }
  _navigate = (route, params) => {
    this.props.navigation.navigate(route, params);
  };
  _activeTab = (tab) => {
    this.setState({
      activeTab: tab
    });
  };

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.navigate("DrawerOpen")}>
              <Icon name="menu"/>
            </Button>
          </Left>
          <Body>
          <Title>Dat Xanh Mien Bac</Title>
          </Body>
          <Right />
        </Header>

        <Tabs renderTabBar={() => <ScrollableTab/>}>
          <Tab heading={<View><Icon name="home"/></View>}>
            <Content>
              <Swiper autoplay={true} style={{ height: 170 }}>
                <View style={{ flex: 1 }}>
                  <Image style={styles.swipingImage}
                         source={require("../../../assets/DXMB/3.jpg")}
                  />
                </View>
                <View style={{ flex: 1 }}>
                  <Image style={styles.swipingImage}
                         source={require("../../../assets/DXMB/4.jpg")}
                  />
                </View>
              </Swiper>
              {/*Start News tab*/}
              <Tabs>
                <Tab heading="Nổi bật">
                  <Content>
                    {/*Start News List*/}
                    <View>
                      <TouchableOpacity>
                        <List
                          dataArray={datas}
                          renderRow={data =>
                            <ListItem thumbnail onPress={() => this._navigate("HomeNewsDetails", { id: data.id, title: data.title })}>
                              <Left>
                                <Thumbnail square size={55} source={data.img}/>
                              </Left>
                              <Body>
                              <Text>
                                {data.title}
                              </Text>
                              <Text numberOfLines={1} note>
                                {data.date}
                              </Text>
                              </Body>
                            </ListItem>
                          }
                        />
                      </TouchableOpacity>
                    </View>
                    {/*End News List*/}
                  </Content>
                </Tab>
                <Tab heading="Thị trường">
                  {/*Start News List*/}
                  <View>
                    <TouchableOpacity>
                      <List
                        dataArray={datas}
                        renderRow={data =>
                          <ListItem thumbnail onPress={() => this._navigate("HomeNewsDetails", { id: data.id, title: data.title })}>
                            <Left>
                              <Thumbnail square size={55} source={data.img}/>
                            </Left>
                            <Body>
                            <Text>
                              {data.title}
                            </Text>
                            <Text numberOfLines={1} note>
                              {data.date}
                            </Text>
                            </Body>
                          </ListItem>
                        }
                      />
                    </TouchableOpacity>
                  </View>
                  {/*End News List*/}
                </Tab>
                <Tab heading="Tiến độ">
                  {/*Start News List*/}
                  <View>
                    <TouchableOpacity>
                      <List
                        dataArray={datas}
                        renderRow={data =>
                          <ListItem thumbnail onPress={() => this._navigate("HomeNewsDetails", { id: data.id, title: data.title })}>
                            <Left>
                              <Thumbnail square size={55} source={data.img}/>
                            </Left>
                            <Body>
                            <Text>
                              {data.title}
                            </Text>
                            <Text numberOfLines={1} note>
                              {data.date}
                            </Text>
                            </Body>
                          </ListItem>
                        }
                      />
                    </TouchableOpacity>
                  </View>
                  {/*End News List*/}
                </Tab>
              </Tabs>
              {/*End News tab*/}
              {/*Start Projects tab*/}
              <Tabs>
                <Tab heading={"Đang bán"}>
                  <TouchableOpacity>
                    <List dataArray={projects} renderRow={project =>
                      <ListItem thumbnail onPress={() => this._navigate("HomeNewsDetails", { id: project.id })}>
                        <Left>
                          <Thumbnail square size={55} source={project.img}/>
                        </Left>
                        <Body>
                        <Text>
                          {project.title}
                        </Text>
                        <Text numberOfLines={1} note>
                          <Icon style={styles.iconSM} type="FontAwesome" name="map-marker"/> {project.address}
                        </Text>
                        <Text numberOfLines={1}>
                          <Icon type="FontAwesome" name="usd" style={styles.iconSM}/> Giá: {project.price}
                        </Text>
                        </Body>
                      </ListItem>
                    }/>
                    <ListItem>
                      <Left>
                        <Text>
                          Xem tất cả
                        </Text>
                      </Left>
                      <Right>
                        <Icon name="arrow-forward"/>
                      </Right>
                    </ListItem>
                  </TouchableOpacity>
                </Tab>
                <Tab heading={"Cho thuê"}>
                  <Text>def</Text>
                </Tab>
              </Tabs>
              {/*End Projects tab*/}
              {/*Start Contact*/}
              <Tabs>
                <Tab heading={"THÔNG TIN LIÊN HỆ"}>
                  <List>
                    <ListItem icon>
                      <Left>
                        <Icon name="card"/>
                      </Left>
                      <Body>
                      <Text>Công ty cổ phần dịch vụ và địa ốc Đất Xanh Miền Bắc</Text>
                      </Body>
                    </ListItem>
                    <ListItem icon>
                      <Left>
                        <Icon name="navigate"/>
                      </Left>
                      <Body><Text>Tầng 10, Tòa nhà Center Building, Số 1 Nguyễn Huy Tưởng, QuậnThanh Xuân,Hà Nội</Text></Body>
                    </ListItem>
                    <ListItem icon>
                      <Left>
                        <Icon name="call"/>
                      </Left>
                      <Body><Text>0917 612 020 - (024) 626 888 11</Text></Body>
                    </ListItem>
                    <ListItem icon>
                      <Left>
                        <Icon name="link"/>
                      </Left>
                      <Body><Text>Website: https://datxanhmienbac.com.vn/</Text></Body>
                    </ListItem>
                  </List>
                </Tab>
              </Tabs>
              {/*End Contact*/}
              {/*Start exchanges */}
              <Tabs>
                <Tab heading={"SÀN GIAO DỊCH"}>
                  <Card>
                    <CardItem cardBody>
                      <Image
                        source={{ uri: "https://datxanhmienbac.com.vn/public/uploads/source/tin-noi-bo/ky-ket-hop-tac-dxmb-dfc.jpg" }}
                        style={{ height: 200, width: null, flex: 1 }}/>
                    </CardItem>
                    <CardItem>
                      <Body>
                      <H3>SÀN HỘI SỞ</H3>
                      <Text note><Icon name={"map-marker"} type={"FontAwesome"} style={styles.iconSM}/> Tầng 18, Center Building, 1 Nguyễn Huy
                        Tưởng, Thanh Xuân,Hà Nội</Text>
                      </Body>
                    </CardItem>
                  </Card>
                  <Card>
                    <CardItem cardBody>
                      <Image
                        source={{ uri: "https://datxanhmienbac.com.vn/public/uploads/source/tin-noi-bo/tap-doan-dat-xanh-dai-ngo-nhan-tai-2.jpg" }}
                        style={{ height: 200, width: null, flex: 1 }}/>
                    </CardItem>
                    <CardItem>
                      <Left>
                        <Body>
                        <H3>SÀN HỘI SỞ</H3>
                        <Text note><Icon name={"map-marker"} type={"FontAwesome"} style={styles.iconSM}/> Tầng 18, Center Building, 1 Nguyễn
                          Huy
                          Tưởng, Thanh Xuân,Hà Nội</Text>
                        </Body>
                      </Left>
                    </CardItem>
                  </Card>
                </Tab>
              </Tabs>
            </Content>
          </Tab>
          {/**** HOME TAB ******/}
          <Tab heading="Giới thiệu">
            <TabAbout/>
          </Tab>
          <Tab heading="Dự án">
            <ProjectList/>
          </Tab>
          <Tab heading="Tin tức">
            <TabNews/>
          </Tab>
          <Tab heading="Tư vấn">
            <TabServiceCare/>
          </Tab>
          <Tab heading="Liên hệ">
            <TabContact/>
          </Tab>
        </Tabs>
      </Container>
    );
  }
}

export default Home;
