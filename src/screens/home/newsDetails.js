import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Spinner
} from "native-base";
import styles from "./styles";
import { Dimensions, Image, WebView } from "react-native";

const deviceWidth = Dimensions.get("window").width;
const data = {
  img: require("../../../assets/DXMB/1.jpg"),
  title: "Cách chọn nhà trong thời đại ăn phải ngon, mặc phải đẹp",
  date: "12/06/2017",
  id: "1"
};

export default class HomeNewsDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {loading: true};
    const id = this.props.navigation.state.params.id;
    console.log("ID: " + id);
  }
  _spinning = ()=>{
    return (<Spinner color={"blue"}/>);
  };

  render() {
    const title = this.props.navigation.state.params.title;
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back"/>
            </Button>
          </Left>
          <Body>
          <Title>{title}</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <WebView source={{uri: "https://datxanhmienbac.com.vn/tin-tuc/sun-group-tang-ky-nghi-duong-cho-nha-dau-tu-shophouse-sun-plaza-grand-world.html"}}
                   style={{width: deviceWidth, marginTop: -80, height:3300}}
                   renderLoading={this._spinning}
                   startInLoadingState={true}
                   scalesPageToFit={true}
          />
        </Content>
      </Container>
    );
  }
}
