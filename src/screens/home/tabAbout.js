import React, { Component } from "react";
import { Content } from "native-base";
import { Dimensions, WebView } from "react-native";

const deviceWidth = Dimensions.get("window").width;

export default class TabAbout extends Component {
  render() {
    return (
      <Content padder>
        <WebView source={{uri: "https://datxanhmienbac.com.vn/gioi-thieu/ve-dat-xanh"}}
                 style={{width: deviceWidth, marginTop: -80, height:6300}}
        />
      </Content>
    );
  }
}
