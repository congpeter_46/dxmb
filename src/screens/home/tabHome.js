import React, { Component } from "react";
import {
  Image,
  TouchableOpacity,
  View
} from "react-native";
import {
  Button,
  Content,
  Left,
  Right,
  Text,
  Body,
  List,
  ListItem,
  Thumbnail,
  Tabs,
  Tab
} from "native-base";
import Swiper from "react-native-swiper";
import styles from "./styles";

import HomeNewsDetails from "./newsDetails";

const datas = [
  {
    img: require("../../../assets/DXMB/1.jpg"),
    text: "  Cách chọn nhà trong thời đại ăn phải ngon, mặc phải đẹp",
    date: "12/06/2017",
    route: "BasicCard"
  },
  {
    img: require("../../../assets/DXMB/2.jpg"),
    text: "Khi mua nhà không còn là việc mua trên giấy tờ",
    date: "11/09/2017",
    route: "HomeNewsDetails"
  },
  {
    img: require("../../../assets/DXMB/3.jpg"),
    text: "Bất động sản đầu năm 2018: Đất vàng Hồ Tây tiếp tục hút khách",
    date: "12/01/2018",
    route: "RegularActionSheet"
  },
  {
    img: require("../../../assets/DXMB/4.jpg"),
    text: "Hiểu đúng về condotel: Để khách hàng khỏi thiệt",
    date: "12/06/2017",
    route: "NewsDetails"
  }
];

export default class TabHome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      parentNav: ""
    };
  }
  componentWillReceiveProps(parentNav) {
    // update original states
    this.setState({
      parentNav: parentNav
    });
  }
  render() {
    const parentNav = (route) => {this.state.parentNav(route)};
    console.log(parentNav);
    console.log(">>>>>>>>>>>>>>>>>>>>>");
    return (
      <Content>
        <Swiper autoplay={true} style={{ height: 170 }}>
          <View style={{ flex: 1 }}>
            <Image style={styles.swipingImage}
                   source={require("../../../assets/DXMB/3.jpg")}
            />
          </View>
          <View style={{ flex: 1 }}>
            <Image style={styles.swipingImage}
                   source={require("../../../assets/DXMB/4.jpg")}
            />
          </View>
        </Swiper>
        {/*Start News tab*/}
        <Tabs>
          <Tab heading="Tab1">
            <Content padder>
              <Text>Tab 1</Text>
              <Text>iOS splash screen status bar is white in standalone apps but dark in Expo client. It should be dark
                in
                standalone apps by default too, and also it should be customizable</Text>
            </Content>
          </Tab>
          <Tab heading="Tab2">
            <Text>Tab 2</Text>

          </Tab>
          <Tab heading="Tab3">
            <Text>Tab 3</Text>

          </Tab>
        </Tabs>
        {/*End News tab*/}
        {/*Start News List*/}
        <View>
          <List
            dataArray={datas}
            renderRow={data =>
                <ListItem thumbnail onPress={() => parentNav(data.route)}>
                  <Left>
                    <Thumbnail square size={55} source={data.img}/>
                  </Left>
                  <Body>
                  <Text>
                    {data.route}
                  </Text>
                  <Text numberOfLines={1} note>
                    {data.date}
                  </Text>
                  </Body>
                </ListItem>}
          />
        </View>
        {/*End News List*/}
      </Content>
    );
  }
}
