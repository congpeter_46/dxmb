const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceWidth = Dimensions.get("window").width;
export default {
  container: {
    backgroundColor: "#FFF"
  },
  text: {
    alignSelf: "center",
    marginBottom: 7
  },
  mb: {
    marginBottom: 15
  },
  cardImage: {
    alignSelf: "center",
    height: 150,
    resizeMode: "cover",
    width: deviceWidth / 1.18,
    marginVertical: 5
  }
};
