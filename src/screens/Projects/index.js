import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Input,
  Item,
  Text,
  Right,
  Body,
  Left,
  List,
  ListItem,
  Tabs,
  Tab,
  Thumbnail,
  View
} from "native-base";
import {TouchableOpacity} from "react-native";
import styles from "../home/styles";

const datas = [
  {
    img: require("../../../assets/DXMB/3.jpg"),
    title: "Chung cư cao cấp Imperial Plaza",
    address: "360 Giải Phóng",
    price: "25.5 triệu/m2",
    id: "1"
  },
  {
    img: require("../../../assets/DXMB/1.jpg"),
    title: "Chung cư Hateco Apollo Xuân Phương",
    address: "Phường Phương Canh, Nam Từ Liêm",
    price: "1.2 tỷ/căn",
    id: "2"
  },
  {
    img: require("../../../assets/DXMB/4.jpg"),
    title: "Chung cư TNR Sky Park",
    address: "136 Hồ Tùng Mậu, Bắc Từ Liêm, Hà Nội",
    price: "24 triệu/m2",
    id: "3"
  },
  {
    img: require("../../../assets/DXMB/2.jpg"),
    title: "Chung cư Roman Plaza Hải Phát",
    address: "Phường Đại Mỗ, Hà Nội",
    price: "27 triệu/m2",
    id: "4"
  }
];

class ProjectList extends Component {
  render() {
    return (
      <Content>
        <Item regular>
          <Icon active name="search" />
          <Input placeholder="Tìm kiếm..." />
          <Button transparent>
            <Icon name="arrow-right" type="FontAwesome" />
          </Button>
        </Item>

        <Tabs>
          <Tab heading="Nổi bật">
              {/*Start Project List*/}
              <View>
                <TouchableOpacity>
                  <List dataArray={datas} renderRow={project =>
                    <ListItem thumbnail onPress={() => this._navigate("HomeNewsDetails", { id: project.id })}>
                      <Left>
                        <Thumbnail square size={55} source={project.img}/>
                      </Left>
                      <Body>
                      <Text>
                        {project.title}
                      </Text>
                      <Text numberOfLines={1} note>
                        <Icon style={styles.iconSM} type="FontAwesome" name="map-marker"/> {project.address}
                      </Text>
                      <Text numberOfLines={1}>
                        <Icon type="FontAwesome" name="usd" style={styles.iconSM}/> Giá: {project.price}
                      </Text>
                      </Body>
                    </ListItem>
                  }/>
                </TouchableOpacity>
              </View>
              {/*End Project List*/}
          </Tab>
          <Tab heading="Miền Bắc">
            {/*Start Project List*/}
            <View>
              <TouchableOpacity>
                <List dataArray={datas} renderRow={project =>
                  <ListItem thumbnail onPress={() => this._navigate("HomeNewsDetails", { id: project.id })}>
                    <Left>
                      <Thumbnail square size={55} source={project.img}/>
                    </Left>
                    <Body>
                    <Text>
                      {project.title}
                    </Text>
                    <Text numberOfLines={1} note>
                      <Icon style={styles.iconSM} type="FontAwesome" name="map-marker"/> {project.address}
                    </Text>
                    <Text numberOfLines={1}>
                      <Icon type="FontAwesome" name="usd" style={styles.iconSM}/> Giá: {project.price}
                    </Text>
                    </Body>
                  </ListItem>
                }/>
              </TouchableOpacity>
            </View>
            {/*End Project List*/}
          </Tab>
          <Tab heading="M.Trung">
            {/*Start Project List*/}
            <View>
              <TouchableOpacity>
                <List dataArray={datas} renderRow={project =>
                  <ListItem thumbnail onPress={() => this._navigate("HomeNewsDetails", { id: project.id })}>
                    <Left>
                      <Thumbnail square size={55} source={project.img}/>
                    </Left>
                    <Body>
                    <Text>
                      {project.title}
                    </Text>
                    <Text numberOfLines={1} note>
                      <Icon style={styles.iconSM} type="FontAwesome" name="map-marker"/> {project.address}
                    </Text>
                    <Text numberOfLines={1}>
                      <Icon type="FontAwesome" name="usd" style={styles.iconSM}/> Giá: {project.price}
                    </Text>
                    </Body>
                  </ListItem>
                }/>
              </TouchableOpacity>
            </View>
            {/*End Project List*/}
          </Tab>
          <Tab heading="Miền Nam">
            {/*Start Project List*/}
            <View>
              <TouchableOpacity>
                <List dataArray={datas} renderRow={project =>
                  <ListItem thumbnail onPress={() => this._navigate("HomeNewsDetails", { id: project.id })}>
                    <Left>
                      <Thumbnail square size={55} source={project.img}/>
                    </Left>
                    <Body>
                    <Text>
                      {project.title}
                    </Text>
                    <Text numberOfLines={1} note>
                      <Icon style={styles.iconSM} type="FontAwesome" name="map-marker"/> {project.address}
                    </Text>
                    <Text numberOfLines={1}>
                      <Icon type="FontAwesome" name="usd" style={styles.iconSM}/> Giá: {project.price}
                    </Text>
                    </Body>
                  </ListItem>
                }/>
              </TouchableOpacity>
            </View>
            {/*End Project List*/}
          </Tab>
        </Tabs>
        <Tabs>
          <Tab heading={"Tất cả các dự án"}>
            <View>
              <TouchableOpacity>
                <List dataArray={datas} renderRow={project =>
                  <ListItem thumbnail onPress={() => this._navigate("HomeNewsDetails", { id: project.id })}>
                    <Left>
                      <Thumbnail square size={55} source={project.img}/>
                    </Left>
                    <Body>
                    <Text>
                      {project.title}
                    </Text>
                    <Text numberOfLines={1} note>
                      <Icon style={styles.iconSM} type="FontAwesome" name="map-marker"/> {project.address}
                    </Text>
                    <Text numberOfLines={1}>
                      <Icon type="FontAwesome" name="usd" style={styles.iconSM}/> Giá: {project.price}
                    </Text>
                    </Body>
                  </ListItem>
                }/>
              </TouchableOpacity>
            </View>
          </Tab>
        </Tabs>
      </Content>
    );
  }
}

export default ProjectList;
