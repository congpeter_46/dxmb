import React, { Component } from "react";
import { Image, TouchableOpacity } from "react-native";
import {
  Body,
  Button,
  Content,
  Text,
  List,
  ListItem,
  Icon,
  Container,
  Left,
  Right,
  Badge,
  Switch
} from "native-base";
import styles from "./style";

const drawerCover = require("../../../assets/drawer-cover.png");
const drawerImage = require("../../../assets/logo.png");
const datas = [
  {
    name: "Home",
    route: "Home",
    icon: "home",
    bg: "#C5F442"
  },
  {
    name: "Anatomy",
    route: "Anatomy",
    icon: "phone-portrait",
    bg: "#C5F442"
  },
  {
    name: "Actionsheet",
    route: "Actionsheet",
    icon: "easel",
    bg: "#C5F442"
  },
  {
    name: "Header",
    route: "Header",
    icon: "phone-portrait",
    bg: "#477EEA",
    types: "10"
  },
  {
    name: "Footer",
    route: "Footer",
    icon: "phone-portrait",
    bg: "#DA4437",
    types: "4"
  },
  {
    name: "Badge",
    route: "NHBadge",
    icon: "notifications",
    bg: "#4DCAE0"
  },
  {
    name: "Button",
    route: "NHButton",
    icon: "radio-button-off",
    bg: "#1EBC7C",
    types: "9"
  },
  {
    name: "Card",
    route: "NHCard",
    icon: "keypad",
    bg: "#B89EF5",
    types: "8"
  },
  {
    name: "Check Box",
    route: "NHCheckbox",
    icon: "checkmark-circle",
    bg: "#EB6B23"
  },
  {
    name: "Deck Swiper",
    route: "NHDeckSwiper",
    icon: "swap",
    bg: "#3591FA",
    types: "2"
  },
  {
    name: "Fab",
    route: "NHFab",
    icon: "help-buoy",
    bg: "#EF6092",
    types: "2"
  },
  {
    name: "Form & Inputs",
    route: "NHForm",
    icon: "call",
    bg: "#EFB406",
    types: "12"
  },
  {
    name: "Icon",
    route: "NHIcon",
    icon: "information-circle",
    bg: "#bfe9ea",
    types: "4"
  },
  {
    name: "Layout",
    route: "NHLayout",
    icon: "grid",
    bg: "#9F897C",
    types: "5"
  },
  {
    name: "List",
    route: "NHList",
    icon: "lock",
    bg: "#5DCEE2",
    types: "8"
  },
  {
    name: "ListSwipe",
    route: "ListSwipe",
    icon: "swap",
    bg: "#C5F442",
    types: "3"
  },
  {
    name: "Picker",
    route: "NHPicker",
    icon: "arrow-dropdown",
    bg: "#F50C75"
  },
  {
    name: "Radio",
    route: "NHRadio",
    icon: "radio-button-on",
    bg: "#6FEA90"
  },
  {
    name: "SearchBar",
    route: "NHSearchbar",
    icon: "search",
    bg: "#29783B"
  },
  {
    name: "Segment",
    route: "Segment",
    icon: "menu",
    bg: "#0A2C6B",
    types: "2"
  },
  {
    name: "Spinner",
    route: "NHSpinner",
    icon: "navigate",
    bg: "#BE6F50"
  },
  {
    name: "Tabs",
    route: "NHTab",
    icon: "home",
    bg: "#AB6AED",
    types: "3"
  },
  {
    name: "Thumbnail",
    route: "NHThumbnail",
    icon: "image",
    bg: "#cc0000",
    types: "2"
  },
  {
    name: "Toast",
    route: "NHToast",
    icon: "albums",
    bg: "#C5F442",
    types: "6"
  },
  {
    name: "Typography",
    route: "NHTypography",
    icon: "paper",
    bg: "#48525D"
  }
];

class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shadowOffsetWidth: 1,
      shadowRadius: 4,
      activePushNotification: true
    };
  }

  _togglePushNotification = () => {
    this.setState(previousState => {
      return { activePushNotification: !previousState.activePushNotification };
    });
  };

  render() {
    return (
      <Container>
        <Content
          bounces={false}
          style={{ flex: 1, backgroundColor: "#fff", top: -1 }}
        >
          <Text style={styles.drawerCover}/>
          <Image square style={styles.drawerImage} source={drawerImage}/>
          <TouchableOpacity>
            <ListItem icon onPress={this._togglePushNotification}>
              <Left>
                <Button style={{ backgroundColor: "#FF9501" }}>
                  <Icon active name="notifications"/>
                </Button>
              </Left>
              <Body>
              <Text>Nhận thông báo</Text>
              </Body>
              <Right>
                <Switch value={this.state.activePushNotification} onValueChange={this._togglePushNotification}
                        onTintColor="#50B948"/>
              </Right>
            </ListItem>
            <ListItem icon last>
              <Left>
                <Button style={{ backgroundColor: "#4CDA64" }}>
                  <Icon name="mail"/>
                </Button>
              </Left>
              <Body>
              <Text>Đăng ký nhận tin qua email</Text>
              <Text note>Nhận tin tức qua hộp thư điện tử</Text>
              </Body>
            </ListItem>
          </TouchableOpacity>
          <ListItem itemDivider>
            <Text>Thành viên</Text>
          </ListItem>
          <TouchableOpacity>
            <ListItem icon onPress={() => this.props.navigation.navigate("Register")}>
              <Left>
                <Button style={{ backgroundColor: "#5855D6" }}>
                  <Icon name="person-add"/>
                </Button>
              </Left>
              <Body>
              <Text>Đăng ký CTV</Text>
              <Text note>Trở thành CTV của Đất Xanh</Text>
              </Body>
            </ListItem>
            <ListItem icon last onPress={() => this.props.navigation.navigate("Login")}>
              <Left>
                <Button style={{ backgroundColor: "#007AFF" }}>
                  <Icon name="log-in"/>
                </Button>
              </Left>
              <Body>
              <Text>Đăng nhập</Text>
              </Body>
            </ListItem>
            <ListItem icon last onPress={() => this.props.navigation.navigate("AnnouncementList")}>
              <Left>
                <Button style={{ backgroundColor: "#007AFF" }}>
                  <Icon name="list"/>
                </Button>
              </Left>
              <Body>
              <Text>Tin nội bộ</Text>
              </Body>
            </ListItem>
          </TouchableOpacity>
          <ListItem itemDivider icon noBorder last>
            <Left>
              <Icon name={"copyright"} type={"FontAwesome"} style={[styles.textSM, { marginLeft: 15 }]}/>
            </Left>
            <Body>
            <Text style={styles.textSM}>Bản quyền thuộc Đất Xanh Miền Bắc</Text>
            </Body>
          </ListItem>
          <ListItem itemDivider icon noBorder last>
            <Left>
              <Icon name={"navigate"} style={[styles.textSM, { marginLeft: 15 }]}/>
            </Left>
            <Body>
            <Text style={styles.textSM}>Tầng 10 tòa Building Center, số 1 Nguyễn Huy Tưởng</Text>
            </Body>
          </ListItem>
          <ListItem itemDivider icon noBorder last>
            <Left>
              <Icon name={"mail"} style={[styles.textSM, { marginLeft: 15 }]}/>
            </Left>
            <Body>
            <Text style={styles.textSM}>lienhe@dxmb.vn</Text>
            </Body>
          </ListItem>

          <List
            dataArray={datas}
            renderRow={data =>
              <ListItem
                button
                noBorder
                onPress={() => this.props.navigation.navigate(data.route)}
              >
                <Left>
                  <Icon
                    active
                    name={data.icon}
                    style={{ color: "#777", fontSize: 26, width: 30 }}
                  />
                  <Text style={styles.text}>
                    {data.name}
                  </Text>
                </Left>
                {data.types &&
                <Right style={{ flex: 1 }}>
                  <Badge
                    style={{
                      borderRadius: 3,
                      height: 25,
                      width: 72,
                      backgroundColor: data.bg
                    }}
                  >
                    <Text
                      style={styles.badgeText}
                    >{`${data.types} Types`}</Text>
                  </Badge>
                </Right>}
              </ListItem>}
          />
        </Content>
      </Container>
    );
  }
}

export default SideBar;
